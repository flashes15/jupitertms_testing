import json
import os
import time
from AutomatedTestingTMS import generate_CFG
#from AD2RunPattern import *
#from Calculations import *
from AD2RunPattern import initializeAD2,AD2_Output_Select
from Calculations import waveform_Calculations


def get_seg_Length(m):
    SegLens=['10u','20u','50u','100u','200u','500u','1m','2m','5m','10m','20m','50m','100m','200m','500m','1000m','2000m']
    #SegLens=['8u','20u','40u','80u','200u','400u','1m','2m','4m','8m','20m','40m','80m','200m','400m','800m','2000m']
    return SegLens[m]
    
def get_Pretrigger(m):
    return str(m*10)
    
def get_Trigger_Mode(m):
    if m == 0:
        return "positive"
    elif m == 1:
        return "negative"
    elif m == 2:
        return "window-enter"
    elif m == 3:
        return "window-exit"
    else:
        return 0

def get_Impedance(m):
    if m == 0:
        return "50ohm"
    elif m == 1:
        return "1mohm"
    else:
        return 0

def get_Coupling(m):
    if m == 0:
        return "DC"
    elif m == 1:
        return "AC"
    elif m == 2:
        return "GND"    
    else:
        return 0

def get_Range(m):
    if m == 0:
        return "0.2"
    elif m == 1:
        return "2"
    elif m == 2:
        return "20"
    elif m == 3:
        return "200"
    else:
        return 0
       


def update_Config(TMS_Cfg):
    config = {
    "global": {
        "config_filename": "TMS_Test",
        "sample_rate": 80000000,
        "segment_length": get_seg_Length(TMS_Cfg['Seg_Len']),
        "trigger_percentage": get_Pretrigger(TMS_Cfg['Pre_Trig']),
        "triggers": str(TMS_Cfg['Num_Trig']+1),
        "install_location": TMS_Cfg['Header_Name'],
        "trfileprefix": "TMS-19-1"
        },
    "debug": {
        "verbose": True,
        "very_verbose": False,
        "source": False,
        "adc_pattern": "0000",
        "enable_dither": False,
        "enable_chopper": False,
        "enable_sysref": False,
        "disable_spi": False,
        "align_pattern": False       
        },
    "channels": [
        {
            "acquisition": "0",
            "spdclamp": "0",
            "name": "Ch%d" % i,
            "number": "%d" % i,
            "units": "V",
            "multiplier": "1",
            "level_a": str(TMS_Cfg['Ch%d' % i]['TrgA']),
            "level_b": str(TMS_Cfg['Ch%d' % i]['TrgB']),
            "hysteresis": str(TMS_Cfg['Ch%d' % i]['Hyst']),
            "mode": get_Trigger_Mode(TMS_Cfg['Ch%d' % i]['TrigMode']),
            "impedance": get_Impedance(TMS_Cfg['Ch%d' % i]['Zin']),
            "signalcoupling": get_Coupling(TMS_Cfg['Ch%d' % i]['CPL']),
            "inputcoupling": "se",
            "extattenuation": "0",
            "range": get_Range(TMS_Cfg['Ch%d' % i]['Range']),
            "or": False if TMS_Cfg['Ch%d' % i]['TrigOr'] == 0 else True,
            "and": False if TMS_Cfg['Ch%d' % i]['TrigAnd'] ==0 else True,
        } for i in range(1,5)        
        ]
    }
    
    filename = '/opt/TMSTesting/config/config.json'
    
    with open(filename, 'w') as f:
        f.write(json.dumps(config,indent=2))
    
    return 0


def transfer_Config():
    
    os.system('ssh-keygen -f "/Users/SLS/.ssh/known_hosts" -R 192.168.1.70')
    os.system('/usr/local/bin/sshpass -p "SLSL1ghtn1ngTMS2018A!" scp -o StrictHostKeyChecking=no /opt/TMSTesting/config/config.json root@192.168.1.70:/media/config/')   
    
    return 0


#Initialize the AD2
dwf, hdwf = initializeAD2()


#generate_CFG(numTrig,segLen, preTrig, trigA, trigB, trigMode, trigOR, trigAND, Hyst)


#Main selector for test type
selector = 0


#Zerodeadtime, change pre-trigger and record length 
#####################################################################################################################################
if selector == 0:
    
    #Iterate over pre-trigger values
    for j in range(5,6):
     
        #From 8us to 2ms
        for i in range(1,2):
            #Call the method that contains the TMS setup parameters
            TMS_Cfg = generate_CFG(selector,10,i,j,[.1,.1,.1,.1],[.1,.1,.1,.1],[0,0,0,0],[1,0,0,0],[0,0,0,0],[0,0,0,0])
                    
            #Generate the JSON
            update_Config(TMS_Cfg)
                    
            #Send config to TMS
            transfer_Config()
                    
            #Wait for TMS to arm 
            time.sleep(12)
                    
            #Trigger TMS
            AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'],[0],TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
                    
            #Call calculation routine
            waveform_Calculations(TMS_Cfg)
               
#         #From 5ms to 100ms
#         for i in range(8,13):
#             #Call the method that contains the TMS setup parameters
#             TMS_Cfg = generate_CFG(selector,3,i,j,[.1,.1,.1,.1],[.1,.1,.1,.1],[0,0,0,0],[1,0,0,0],[0,0,0,0],[0,0,0,0])
#                  
#             #Generate the JSON
#             update_Config(TMS_Cfg)
#                  
#             #Send config to TMS
#             transfer_Config()
#                  
#             #Wait for TMS to arm 
#             time.sleep(12)
#                  
#             #Trigger TMS
#             AD2_Output_Select(dwf, hdwf, TMS_Cfg['Test_Type'],[0], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
#                  
#             #Call calculation routine
#             waveform_Calculations(TMS_Cfg)
#                
#         #From 200ms to 1000ms
#         for i in range(13,16):
#             #Call the method that contains the TMS setup parameters
#             TMS_Cfg = generate_CFG(selector,3,i,j,[.1,.1,.1,.1],[.1,.1,.1,.1],[0,0,0,0],[1,0,0,0],[0,0,0,0],[0,0,0,0])
#                
#             #Generate the JSON
#             update_Config(TMS_Cfg)
#                
#             #Send config to TMS
#             transfer_Config()
#                
#             #Wait for TMS to arm 
#             time.sleep(12)
#                
#             #Trigger TMS
#             AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'],[0], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
#              
#             #Call calculation routine
#             waveform_Calculations(TMS_Cfg)    
              
#         #2s
#           
#         #Call the method that contains the TMS setup parameters
#         TMS_Cfg = generate_CFG(selector,2,16,j,[.1,.1,.1,.1],[.1,.1,.1,.1],[0,0,0,0],[1,1,1,1],[0,0,0,0],[0,0,0,0])
#           
#         #Generate the JSON
#         update_Config(TMS_Cfg)
#           
#         #Send config to TMS
#         transfer_Config()
#           
#         #Wait for TMS to arm 
#         time.sleep(12)
#           
#         #Trigger TMS
#         AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'],[0], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
#           
#         #Call calculation routine
#         waveform_Calculations(TMS_Cfg)  
#####################################################################################################################################        
        
#Trigger mode check +Channels
#####################################################################################################################################
elif selector == 1:
    
    ########################################## 
    #Test Positive triggers on all +channels #
    ##########################################
      
    #Channel 1+
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[.1,.1,.1,.1],[0,0,0,0],[1,0,0,0],[0,0,0,0],[0,0,0,0])
         
    #Generate the JSON
    update_Config(TMS_Cfg)
                 
    #Send config to TMS
    transfer_Config()
                 
    #Wait for TMS to arm 
    time.sleep(12)
                 
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf, TMS_Cfg['Test_Type'],[0], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
             
         
    #Channel 2+
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[.1,.1,.1,.1],[0,0,0,0],[0,1,0,0],[0,0,0,0],[0,0,0,0])
         
    #Generate the JSON
    update_Config(TMS_Cfg)
            
    #Send config to TMS
    transfer_Config()
            
    #Wait for TMS to arm 
    time.sleep(12)
            
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'],[2], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
         
         
    #Channel 3+
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[.1,.1,.1,.1],[0,0,0,0],[0,0,1,0],[0,0,0,0],[0,0,0,0])
         
    #Generate the JSON
    update_Config(TMS_Cfg)
            
    #Send config to TMS
    transfer_Config()
            
    #Wait for TMS to arm 
    time.sleep(12)
            
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [4], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
         
    #Channel 4+
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[.1,.1,.1,.1],[0,0,0,0],[0,0,0,1],[0,0,0,0],[0,0,0,0])
         
    #Generate the JSON
    update_Config(TMS_Cfg)
            
    #Send config to TMS
    transfer_Config()
            
    #Wait for TMS to arm 
    time.sleep(12)
            
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [6], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig']) 
       
    ########################################## 
    #Test Negative triggers on all +channels #
    ##########################################
      
    #Channel 1+
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[.1,.1,.1,.1],[1,1,1,1],[1,0,0,0],[0,0,0,0],[0,0,0,0])
       
    #Generate the JSON
    update_Config(TMS_Cfg)
               
    #Send config to TMS
    transfer_Config()
               
    #Wait for TMS to arm 
    time.sleep(12)
               
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [0], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
           
        
    #Channel 2+
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[.1,.1,.1,.1],[1,1,1,1],[0,1,0,0],[0,0,0,0],[0,0,0,0])
         
    #Generate the JSON
    update_Config(TMS_Cfg)
            
    #Send config to TMS
    transfer_Config()
            
    #Wait for TMS to arm 
    time.sleep(12)
            
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [2], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
         
         
    #Channel 3+
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[.1,.1,.1,.1],[1,1,1,1],[0,0,1,0],[0,0,0,0],[0,0,0,0])
         
    #Generate the JSON
    update_Config(TMS_Cfg)
            
    #Send config to TMS
    transfer_Config()
            
    #Wait for TMS to arm 
    time.sleep(12)
            
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [4], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
         
    #Channel 4+
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[.1,.1,.1,.1],[1,1,1,1],[0,0,0,1],[0,0,0,0],[0,0,0,0])
         
    #Generate the JSON
    update_Config(TMS_Cfg)
            
    #Send config to TMS
    transfer_Config()
            
    #Wait for TMS to arm 
    time.sleep(12)
            
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [6], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig']) 
     
    ############################################### 
    # Test Window-Enter triggers on all +channels #
    ###############################################
    
    #Channel 1+
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],8*[-.1,-.1,-.1,-.1],[2,2,2,2],[1,0,0,0],[0,0,0,0],[0,0,0,0])
     
    #Generate the JSON
    update_Config(TMS_Cfg)
             
    #Send config to TMS
    transfer_Config()
             
    #Wait for TMS to arm 
    time.sleep(12)
             
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [0], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
         
      
    #Channel 2+
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],8*[-.1,-.1,-.1,-.1],[2,2,2,2],[0,1,0,0],[0,0,0,0],[0,0,0,0])
       
    #Generate the JSON
    update_Config(TMS_Cfg)
          
    #Send config to TMS
    transfer_Config()
          
    #Wait for TMS to arm 
    time.sleep(12)
          
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [2], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
       
       
    #Channel 3+
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],8*[-.1,-.1,-.1,-.1],[2,2,2,2],[0,0,1,0],[0,0,0,0],[0,0,0,0])
       
    #Generate the JSON
    update_Config(TMS_Cfg)
          
    #Send config to TMS
    transfer_Config()
          
    #Wait for TMS to arm 
    time.sleep(12)
          
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [4], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
       
    #Channel 4+
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],8*[-.1,-.1,-.1,-.1],[2,2,2,2],[0,0,0,1],[0,0,0,0],[0,0,0,0])
       
    #Generate the JSON
    update_Config(TMS_Cfg)
          
    #Send config to TMS
    transfer_Config()
          
    #Wait for TMS to arm 
    time.sleep(12)
          
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [6], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig']) 
    
    ############################################### 
    # Test Window-Exit triggers on all +channels  #
    ###############################################
    
    #Channel 1+
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[1,0,0,0],[0,0,0,0],[0,0,0,0])
     
    #Generate the JSON
    update_Config(TMS_Cfg)
             
    #Send config to TMS
    transfer_Config()
             
    #Wait for TMS to arm 
    time.sleep(12)
             
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [0], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
         
      
    #Channel 2+
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[0,1,0,0],[0,0,0,0],[0,0,0,0])
       
    #Generate the JSON
    update_Config(TMS_Cfg)
          
    #Send config to TMS
    transfer_Config()
          
    #Wait for TMS to arm 
    time.sleep(12)
          
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [2], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
       
       
    #Channel 3+
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[0,0,1,0],[0,0,0,0],[0,0,0,0])
       
    #Generate the JSON
    update_Config(TMS_Cfg)
          
    #Send config to TMS
    transfer_Config()
          
    #Wait for TMS to arm 
    time.sleep(12)
          
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [4], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
       
    #Channel 4+
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[0,0,0,1],[0,0,0,0],[0,0,0,0])
       
    #Generate the JSON
    update_Config(TMS_Cfg)
          
    #Send config to TMS
    transfer_Config()
          
    #Wait for TMS to arm 
    time.sleep(12)
          
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [6], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig']) 
    
     
    #Call calculation routine
    waveform_Calculations(TMS_Cfg)    
              
#Trigger mode check -Channels
#####################################################################################################################################
elif selector == 2:
    
    ########################################## 
    #Test Positive triggers on all -channels #
    ##########################################
      
    #Channel 1-
    TMS_Cfg = generate_CFG(selector,1,4,5,[-.1,-.1,-.1,-.1],[-.1,-.1,-.1,-.1],[0,0,0,0],[1,0,0,0],[0,0,0,0],[0,0,0,0])
         
    #Generate the JSON
    update_Config(TMS_Cfg)
                 
    #Send config to TMS
    transfer_Config()
                 
    #Wait for TMS to arm 
    time.sleep(12)
                 
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf, TMS_Cfg['Test_Type'],[1], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
             
         
    #Channel 2-
    TMS_Cfg = generate_CFG(selector,1,4,5,[-.1,-.1,-.1,-.1],[-.1,-.1,-.1,-.1],[0,0,0,0],[0,1,0,0],[0,0,0,0],[0,0,0,0])
          
    #Generate the JSON
    update_Config(TMS_Cfg)
             
    #Send config to TMS
    transfer_Config()
             
    #Wait for TMS to arm 
    time.sleep(12)
             
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'],[3], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
          
          
    #Channel 3-
    TMS_Cfg = generate_CFG(selector,1,4,5,[-.1,-.1,-.1,-.1],[-.1,-.1,-.1,-.1],[0,0,0,0],[0,0,1,0],[0,0,0,0],[0,0,0,0])
          
    #Generate the JSON
    update_Config(TMS_Cfg)
             
    #Send config to TMS
    transfer_Config()
             
    #Wait for TMS to arm 
    time.sleep(12)
             
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [5], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
          
    #Channel 4-
    TMS_Cfg = generate_CFG(selector,1,4,5,[-.1,-.1,-.1,-.1],[-.1,-.1,-.1,-.1],[0,0,0,0],[0,0,0,1],[0,0,0,0],[0,0,0,0])
          
    #Generate the JSON
    update_Config(TMS_Cfg)
             
    #Send config to TMS
    transfer_Config()
             
    #Wait for TMS to arm 
    time.sleep(12)
             
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [7], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig']) 
       
    ########################################## 
    #Test Negative triggers on all -channels #
    ##########################################
       
    #Channel 1-
    TMS_Cfg = generate_CFG(selector,1,4,5,[-.1,-.1,-.1,-.1],[-.1,-.1,-.1,-.1],[1,1,1,1],[1,0,0,0],[0,0,0,0],[0,0,0,0])
        
    #Generate the JSON
    update_Config(TMS_Cfg)
                
    #Send config to TMS
    transfer_Config()
                
    #Wait for TMS to arm 
    time.sleep(12)
                
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [1], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
            
         
    #Channel 2-
    TMS_Cfg = generate_CFG(selector,1,4,5,[-.1,-.1,-.1,-.1],[-.1,-.1,-.1,-.1],[1,1,1,1],[0,1,0,0],[0,0,0,0],[0,0,0,0])
          
    #Generate the JSON
    update_Config(TMS_Cfg)
             
    #Send config to TMS
    transfer_Config()
             
    #Wait for TMS to arm 
    time.sleep(12)
             
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [3], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
          
          
    #Channel 3-
    TMS_Cfg = generate_CFG(selector,1,4,5,[-.1,-.1,-.1,-.1],[-.1,-.1,-.1,-.1],[1,1,1,1],[0,0,1,0],[0,0,0,0],[0,0,0,0])
          
    #Generate the JSON
    update_Config(TMS_Cfg)
             
    #Send config to TMS
    transfer_Config()
             
    #Wait for TMS to arm 
    time.sleep(12)
             
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [5], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
          
    #Channel 4-
    TMS_Cfg = generate_CFG(selector,1,4,5,[-.1,-.1,-.1,-.1],[-.1,-.1,-.1,-.1],[1,1,1,1],[0,0,0,1],[0,0,0,0],[0,0,0,0])
          
    #Generate the JSON
    update_Config(TMS_Cfg)
             
    #Send config to TMS
    transfer_Config()
             
    #Wait for TMS to arm 
    time.sleep(12)
             
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [7], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig']) 
      
    ############################################### 
    # Test Window-Enter triggers on all -channels #
    ###############################################
     
    #Channel 1-
    TMS_Cfg = generate_CFG(selector,1,4,5,8*[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[2,2,2,2],[1,0,0,0],[0,0,0,0],[0,0,0,0])
      
    #Generate the JSON
    update_Config(TMS_Cfg)
              
    #Send config to TMS
    transfer_Config()
              
    #Wait for TMS to arm 
    time.sleep(12)
              
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [1], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
          
       
    #Channel 2-
    TMS_Cfg = generate_CFG(selector,1,4,5,8*[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[2,2,2,2],[0,1,0,0],[0,0,0,0],[0,0,0,0])
        
    #Generate the JSON
    update_Config(TMS_Cfg)
           
    #Send config to TMS
    transfer_Config()
           
    #Wait for TMS to arm 
    time.sleep(12)
           
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [3], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
        
        
    #Channel 3-
    TMS_Cfg = generate_CFG(selector,1,4,5,8*[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[2,2,2,2],[0,0,1,0],[0,0,0,0],[0,0,0,0])
        
    #Generate the JSON
    update_Config(TMS_Cfg)
           
    #Send config to TMS
    transfer_Config()
           
    #Wait for TMS to arm 
    time.sleep(12)
           
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [5], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
        
    #Channel 4-
    TMS_Cfg = generate_CFG(selector,1,4,5,8*[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[2,2,2,2],[0,0,0,1],[0,0,0,0],[0,0,0,0])
        
    #Generate the JSON
    update_Config(TMS_Cfg)
           
    #Send config to TMS
    transfer_Config()
           
    #Wait for TMS to arm 
    time.sleep(12)
           
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [7], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig']) 
     
    ############################################### 
    # Test Window-Exit triggers on all -channels  #
    ###############################################
     
    #Channel 1-
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[1,0,0,0],[0,0,0,0],[0,0,0,0])
      
    #Generate the JSON
    update_Config(TMS_Cfg)
              
    #Send config to TMS
    transfer_Config()
              
    #Wait for TMS to arm 
    time.sleep(12)
              
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [1], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
          
       
    #Channel 2-
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[0,1,0,0],[0,0,0,0],[0,0,0,0])
        
    #Generate the JSON
    update_Config(TMS_Cfg)
           
    #Send config to TMS
    transfer_Config()
           
    #Wait for TMS to arm 
    time.sleep(12)
           
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [3], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
        
        
    #Channel 3-
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[0,0,1,0],[0,0,0,0],[0,0,0,0])
        
    #Generate the JSON
    update_Config(TMS_Cfg)
           
    #Send config to TMS
    transfer_Config()
           
    #Wait for TMS to arm 
    time.sleep(12)
           
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [5], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
    
        
    #Channel 4-
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[0,0,0,1],[0,0,0,0],[0,0,0,0])
        
    #Generate the JSON
    update_Config(TMS_Cfg)
           
    #Send config to TMS
    transfer_Config()
           
    #Wait for TMS to arm 
    time.sleep(12)
           
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [7], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig']) 
     
      
    #Call calculation routine
    waveform_Calculations(TMS_Cfg)     
    
#Logic Trigger check +Channels
#####################################################################################################################################
elif selector == 3: 
    
    #OR Channel 1
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[1,0,0,0],[0,0,0,0],[0,0,0,0])      
    #Generate the JSON
    update_Config(TMS_Cfg)              
    #Send config to TMS
    transfer_Config()              
    #Wait for TMS to arm
    time.sleep(12)              
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [0], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
     
    #OR Channel 2
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[0,1,0,0],[0,0,0,0],[0,0,0,0])      
    #Generate the JSON
    update_Config(TMS_Cfg)              
    #Send config to TMS
    transfer_Config()              
    #Wait for TMS to arm
    time.sleep(12)              
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [2], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
     
    #OR Channel 3
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[0,0,1,0],[0,0,0,0],[0,0,0,0])      
    #Generate the JSON
    update_Config(TMS_Cfg)              
    #Send config to TMS
    transfer_Config()              
    #Wait for TMS to arm
    time.sleep(12)              
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [4], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
     
    #OR Channel 4
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[0,0,0,1],[0,0,0,0],[0,0,0,0])      
    #Generate the JSON
    update_Config(TMS_Cfg)              
    #Send config to TMS
    transfer_Config()              
    #Wait for TMS to arm
    time.sleep(12)              
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [6], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
     
    #AND Ch12
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[0,0,0,0],[1,1,0,0],[0,0,0,0])      
    #Generate the JSON
    update_Config(TMS_Cfg)              
    #Send config to TMS
    transfer_Config()              
    #Wait for TMS to arm
    time.sleep(12)              
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [0,2], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
     
    #AND Ch13
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[0,0,0,0],[1,0,1,0],[0,0,0,0])      
    #Generate the JSON
    update_Config(TMS_Cfg)              
    #Send config to TMS
    transfer_Config()              
    #Wait for TMS to arm
    time.sleep(12)              
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [0,4], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
     
     
    #AND Ch14
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[0,0,0,0],[1,0,0,1],[0,0,0,0])      
    #Generate the JSON
    update_Config(TMS_Cfg)              
    #Send config to TMS
    transfer_Config()              
    #Wait for TMS to arm
    time.sleep(12)              
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [0,6], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
     
    #AND Ch23
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[0,0,0,0],[0,1,1,0],[0,0,0,0])      
    #Generate the JSON
    update_Config(TMS_Cfg)              
    #Send config to TMS
    transfer_Config()              
    #Wait for TMS to arm
    time.sleep(12)              
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [2,4], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
     
    #AND Ch24
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[0,0,0,0],[0,1,0,1],[0,0,0,0])      
    #Generate the JSON
    update_Config(TMS_Cfg)              
    #Send config to TMS
    transfer_Config()              
    #Wait for TMS to arm
    time.sleep(12)              
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [2,6], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
     
    #AND Ch34
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[0,0,0,0],[0,0,1,1],[0,0,0,0])      
    #Generate the JSON
    update_Config(TMS_Cfg)              
    #Send config to TMS
    transfer_Config()              
    #Wait for TMS to arm
    time.sleep(12)              
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [4,6], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
     
    #AND Ch123
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[0,0,0,0],[1,1,1,0],[0,0,0,0])      
    #Generate the JSON
    update_Config(TMS_Cfg)              
    #Send config to TMS
    transfer_Config()              
    #Wait for TMS to arm
    time.sleep(12)              
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [0,2,4], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
     
    #AND Ch124
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[0,0,0,0],[1,1,0,1],[0,0,0,0])      
    #Generate the JSON
    update_Config(TMS_Cfg)              
    #Send config to TMS
    transfer_Config()              
    #Wait for TMS to arm
    time.sleep(12)              
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [0,2,6], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
     
    #AND Ch134
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[0,0,0,0],[1,0,1,1],[0,0,0,0])      
    #Generate the JSON
    update_Config(TMS_Cfg)              
    #Send config to TMS
    transfer_Config()              
    #Wait for TMS to arm
    time.sleep(12)              
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [0,4,6], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
     
    #AND Ch234
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[0,0,0,0],[0,1,1,1],[0,0,0,0])      
    #Generate the JSON
    update_Config(TMS_Cfg)              
    #Send config to TMS
    transfer_Config()              
    #Wait for TMS to arm
    time.sleep(12)              
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [2,4,6], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
     
    #AND Ch1234
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[0,0,0,0],[1,1,1,1],[0,0,0,0])      
    #Generate the JSON
    update_Config(TMS_Cfg)              
    #Send config to TMS
    transfer_Config()              
    #Wait for TMS to arm
    time.sleep(12)              
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [0,2,4,6], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
     
    #OR Ch14 AND Ch23
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[1,0,0,1],[0,1,1,0],[0,0,0,0])      
    #Generate the JSON
    update_Config(TMS_Cfg)              
    #Send config to TMS
    transfer_Config()              
    #Wait for TMS to arm
    time.sleep(12)              
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [0,2,4], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
     
    #OR Ch23 AND Ch14
    TMS_Cfg = generate_CFG(selector,1,4,5,[.1,.1,.1,.1],[-.1,-.1,-.1,-.1],[3,3,3,3],[0,1,1,0],[1,0,0,1],[0,0,0,0])      
    #Generate the JSON
    update_Config(TMS_Cfg)              
    #Send config to TMS
    transfer_Config()              
    #Wait for TMS to arm
    time.sleep(12)              
    #Trigger TMS
    AD2_Output_Select(dwf, hdwf,TMS_Cfg['Test_Type'], [0,2,6], TMS_Cfg['Seg_Len'], TMS_Cfg['Pre_Trig'], TMS_Cfg['Num_Trig'])
    
    
    
    #Call calculation routine
    waveform_Calculations(TMS_Cfg)     
    
    
    
       
dwf.FDwfDeviceCloseAll()        















