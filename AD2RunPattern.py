"""
   DWF Python Example
   Author:  Digilent, Inc.
   Revision:  2018-07-24

   Requires:                       
       Python 2.7, 3
"""

from ctypes import *
import time
import sys


def initializeAD2():
    if sys.platform.startswith("win"):
        dwf = cdll.dwf
    elif sys.platform.startswith("darwin"):
        dwf = cdll.LoadLibrary("/Library/Frameworks/dwf.framework/dwf")
    else:
        dwf = cdll.LoadLibrary("libdwf.so")
    
    version = create_string_buffer(16)    
    dwf.FDwfGetVersion(version)
    print("DWF Version: "+str(version.value))
    
    print("Opening first device")
    hdwf = c_int()
    dwf.FDwfDeviceOpen(c_int(-1), byref(hdwf))
    
    if hdwf.value == 0:
        print("failed to open device")
        szerr = create_string_buffer(512)
        dwf.FDwfGetLastErrorMsg(szerr)
        print(str(szerr.value))
        quit()
        
    return dwf, hdwf   


def segTime(m):
    setTime = [.00001,.00002,.00005,.0001,.0002,.0005,.001,.002,.005,.01,.02,.05,.1,.2,.5,1,2]
    return setTime[m]


#def segTime(m):
    #setTime = [.000008,.00002,.00004,.00008,.0001,.0004,.001,.002,.004,.008,.02,.04,.08,.2,.4,.8,2]
    #return setTime[m]
    

        
def AD2_Output_Select(dwf, hdwf, selector, channel, Seg_Len, Pre_Trig, Num_Trig):   
    
    
    
    
    #Get time length of captured segment
    tseg = segTime(Seg_Len)
         
    
    #Zerodeadtime
    #####################################################################################################################################
    if selector == 0:
                
        #Determine clock divider based on record length. There are only 16000 samples in the pattern generator.
        
        if(tseg<.005):
            #10 MHz sampling rate (1.6 ms total)
            c_divide=10
            
        elif(tseg<.05):
            #1 MHz sampling rate (16 ms total)
            c_divide=100
        elif(tseg<.5):
            #100 KHz sampling rate (160 ms total)
            c_divide=1000
        else:
            #10 KHz sampling rate (1.6 s total)
            c_divide=10000
        
        #Calculate logic high and low intervals in number of samples, scaled to clock division
        high_interval = int((tseg/20)/(c_divide*.00000001))
        #low_interval = int((tseg/2 + .2*tseg)/(c_divide*.00000001))
        low_interval = int((tseg*(1-Pre_Trig/10) + .02*tseg)/(c_divide*.00000001))-high_interval
        
        #.032 (256), .048 (768)
        
        #Calculate total run length in seconds, scaled to clock divider
        run_length = (high_interval + low_interval)*(c_divide*.00000001)        
        
        print(high_interval)
        print(low_interval)
        print(run_length)
        print(c_divide)      
        
        dwf.FDwfDigitalOutDividerSet(hdwf,c_int(channel[0]),c_int(c_divide))
        
        #Set run length the length of the segment
        dwf.FDwfDigitalOutRunSet(hdwf, c_double(run_length))
        
        #Repeat sequence user specified number of time
        dwf.FDwfDigitalOutRepeatSet(hdwf, c_int(Num_Trig)) 
        
        # 1=DwfDigitalOutIdleLow, low output when not running 
        dwf.FDwfDigitalOutIdleSet(hdwf, c_int(channel[0]), c_int(1)) 
        
        # initialize high on start        
        dwf.FDwfDigitalOutCounterInitSet(hdwf, c_int(channel[0]), c_int(0), c_int(0))      
              
        
        dwf.FDwfDigitalOutCounterSet(hdwf, c_int(channel[0]), c_int(low_interval), c_int(high_interval)) 
        
        dwf.FDwfDigitalOutEnableSet(hdwf, c_int(channel[0]), c_int(1))        
              
        print("Generating pulse train...")
        dwf.FDwfDigitalOutConfigure(hdwf, c_int(1))
        
        time.sleep(5)
        
        dwf.FDwfDigitalOutReset(hdwf)  
    
    ##################################################################################################################################### 
        
    #Trigger Mode
    #####################################################################################################################################
    elif selector == 1 or selector == 2:
        
        #Clock divider
        c_divide=10      
        
        #Calculate logic high and low intervals in number of samples, scaled to clock division
        high_interval = int((tseg/20)/(c_divide*.00000001))
        #low_interval = int((tseg/2 + .2*tseg)/(c_divide*.00000001))
        low_interval = int((tseg*(1-Pre_Trig/10) + .05*tseg)/(c_divide*.00000001))-high_interval
        
        #Calculate total run length in seconds, scaled to clock divider
        run_length = (high_interval + low_interval)*(c_divide*.00000001)        
        
        dwf.FDwfDigitalOutDividerSet(hdwf,c_int(channel[0]),c_int(c_divide))
        
        #Set run length the length of the segment
        dwf.FDwfDigitalOutRunSet(hdwf, c_double(run_length))
        
        #Repeat sequence user specified number of time
        dwf.FDwfDigitalOutRepeatSet(hdwf, c_int(Num_Trig)) 
        
        # 1=DwfDigitalOutIdleLow, low output when not running 
        dwf.FDwfDigitalOutIdleSet(hdwf, c_int(channel[0]), c_int(1)) 
        
        # initialize high on start        
        dwf.FDwfDigitalOutCounterInitSet(hdwf, c_int(channel[0]), c_int(0), c_int(0))     
             
        
        dwf.FDwfDigitalOutCounterSet(hdwf, c_int(channel[0]), c_int(low_interval), c_int(high_interval)) 
        
        dwf.FDwfDigitalOutEnableSet(hdwf, c_int(channel[0]), c_int(1))        
              
        print("Generating pulse...")
        dwf.FDwfDigitalOutConfigure(hdwf, c_int(1))
        
        time.sleep(1)

        dwf.FDwfDigitalOutReset(hdwf)  
    
    ##################################################################################################################################### 
        
    #Logic Triggers
    #####################################################################################################################################
        
    elif selector == 3:
        
        #Clock divider
        c_divide=10      
        
        #Calculate logic high and low intervals in number of samples, scaled to clock division
        high_interval = int((tseg/20)/(c_divide*.00000001))
        #low_interval = int((tseg/2 + .2*tseg)/(c_divide*.00000001))
        low_interval = int((tseg*(1-Pre_Trig/10) + .05*tseg)/(c_divide*.00000001))-high_interval
        
        #Calculate total run length in seconds, scaled to clock divider
        run_length = (high_interval + low_interval)*(c_divide*.00000001)   
        
        #Set run length the length of the segment
        dwf.FDwfDigitalOutRunSet(hdwf, c_double(run_length))
        
        #Repeat sequence user specified number of time
        dwf.FDwfDigitalOutRepeatSet(hdwf, c_int(Num_Trig)) 
             
        #Set parameters per channel
        for i in channel:
            dwf.FDwfDigitalOutDividerSet(hdwf,c_int(i),c_int(c_divide))
            dwf.FDwfDigitalOutIdleSet(hdwf, c_int(i), c_int(1)) 
            dwf.FDwfDigitalOutCounterInitSet(hdwf, c_int(i), c_int(0), c_int(0))   
            dwf.FDwfDigitalOutCounterSet(hdwf, c_int(i), c_int(low_interval), c_int(high_interval))  
            dwf.FDwfDigitalOutEnableSet(hdwf, c_int(i), c_int(1))          
                      
        print("Generating pulse...")
        dwf.FDwfDigitalOutConfigure(hdwf, c_int(1))
        
        time.sleep(1)

        dwf.FDwfDigitalOutReset(hdwf)  
        
    else:
        return 0   
        
