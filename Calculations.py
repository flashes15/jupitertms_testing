    
import struct
import time
import sys
import os
import numpy
import glob
import plotly
from plotly.graph_objs import Scatter, Layout


def get_seg_Length(m):
   SegLens=['10u','20u','50u','100u','200u','500u','1m','2m','5m','10m','20m','50m','100m','200m','500m','1000m','2000m']
   return(SegLens[m])
#def get_seg_Length(m):
   # SegLens=['8u','20u','40u','80u','200u','400u','1m','2m','4m','8m','20m','40m','80m','200m','400m','800m','2000m']
    #return(SegLens[m])

def sign_extend(x, b):

    if x&(1<<(b-1)):    # is the highest bit (sign) set? (x>>(b-1)) would be faster
        return x-(1<<b) # 2s complement
    return x


def read_TRFile(capture_filename, read_data):
    raw_capture = True if capture_filename.find('.raw') > -1 else False
    read_header = True
    read_metadata = False
    swap_samples = False
    extend_sign_bit = False    
    num_channels = 4    
    sample_fmt = '<4h'
    sample_len = struct.calcsize(sample_fmt)
    sample_unpack = struct.Struct(sample_fmt).unpack_from    
    hdr = []
    data = [list() for x in range(num_channels)]    
    with open(capture_filename, 'rb') as f:        
        
        if read_header:            #General Settings            h = f.read(39)
            keys = ['headerversion','gpslock', 'timestamp_s', 'timestamp_fsec', 'pretrigger', 'length', 'samplerate']            values = struct.unpack('<10s?QdIII', h)
            hdr = dict(zip(keys, values))             
            h = f.read(62)            keys = ['partnumber', 'serialnumber','softwareversion','installlocation','trfileprefix'] 
            values = struct.unpack('<11s10s10s21s10s', h)
            hdr.update(dict(zip(keys, values)))  
            #Channel 1            h = f.read(102)            keys = ['ch1acqmode','ch1clampvoltage','ch1name','ch1units','ch1inputcoupling','ch1offset','ch1multiplier','ch1triggera','ch1triggerb','ch1triggermode'] 
            values = struct.unpack('<II21s21s5siIddd15s', h)
            hdr.update(dict(zip(keys, values)))             h = f.read(44)            keys = ['ch1hysteresis','ch1inputimpedance','ch1signalcoupling','ch1range','ch1ortrigger','ch1andtrigger']
            values = struct.unpack('<I6s5s15s7s7s',h)
            hdr.update(dict(zip(keys, values)))  
            #Channel 2            h = f.read(102)            keys = ['ch2acqmode','ch2clampvoltage','ch2name','ch2units','ch2offset','ch2multiplier','ch2triggera','ch2triggerb','ch2triggermode']
            values = struct.unpack('<II21s21s5siIddd15s', h)
            hdr.update(dict(zip(keys, values)))             h = f.read(44)            keys = ['ch2hysteresis','ch2inputimpedance','ch2signalcoupling','ch2range','ch2ortrigger','ch2andtrigger']
            values = struct.unpack('<I6s5s15s7s7s',h)
            hdr.update(dict(zip(keys, values)))  
            #Channel 3            h = f.read(102)            keys=['ch3acqmode','ch3clampvoltage','ch3name','ch3units','ch3offset','ch3multiplier','ch3triggera','ch3triggerb','ch3triggermode']
            values = struct.unpack('<II21s21s5siIddd15s', h)
            hdr.update(dict(zip(keys, values)))             h = f.read(44)            keys=['ch3hysteresis','ch3inputimpedance','ch3signalcoupling','ch3range','ch3ortrigger','ch3andtrigger']
            values = struct.unpack('<I6s5s15s7s7s',h)
            hdr.update(dict(zip(keys, values)))  
            #Channel 4            h = f.read(102)            keys=['ch4acqmode','ch4clampvoltage','ch4name','ch4units','ch4offset','ch4multiplier','ch4triggera','ch4triggerb','ch4triggermode']
            values = struct.unpack('<II21s21s5siIddd15s', h)
            hdr.update(dict(zip(keys, values)))             h = f.read(44)            keys = ['ch4hysteresis','ch4inputimpedance','ch4signalcoupling','ch4range','ch4ortrigger','ch4andtrigger']
            values = struct.unpack('<I6s5s15s7s7s',h)            hdr.update(dict(zip(keys, values))) 
                   
        if read_data:
            while True:                s = f.read(sample_len)    
                if not s:    break                d = sample_unpack(s)     
                for i in range(num_channels):                    if extend_sign_bit:                        data[i].append(sign_extend(d[i], 14))                    else:                        data[i].append(d[i])
    return hdr, data               


# def tr_Filewatcher(TMS_Cfg):
#     dataFolder = "/opt/TMSTesting/data"
#     
#     i = inotify.adapters.Inotify()
#     i.add_watch(dataFolder)
# 
#     for event in i.event_gen(yield_nones=False):
#         (_, type_names, path, filename) = event
# 
#         #print("PATH=[{}] FILENAME=[{}] EVENT_TYPES={}".format(
#         #      path, filename, type_names))
# 
#         if type_names == ['IN_CLOSE_WRITE']:
#             waveform_Calculations(TMS_Cfg, path+"/"+filename)
                                      
def waveform_Calculations(TMS_Cfg):                    
    dataFolder = "/opt/TMSTesting/data"
    os.chdir(dataFolder)

    
    #Zerodeadtime
    #####################################################################################################################################
    if TMS_Cfg['Test_Type'] == 0:
                       
        fileList = glob.glob('*.tr')
          
        while len(fileList)<(TMS_Cfg['Num_Trig']):
            print(len(fileList))            
            time.sleep(5)
            print("Waiting for files to transfer from TMS...")            
            fileList = glob.glob('*.tr')    
                
        print(dataFolder + "/" + fileList[len(fileList)-1])        
        
        writing = True    
        
        while writing:
            
            st = os.path.getsize(fileList[len(fileList)-1])
            print(st)
            time.sleep(3)
            nt = os.path.getsize(fileList[len(fileList)-1])
            print(nt)
            
            if nt > st:
                continue            
            elif nt == st:
                writing = False     
 
      
        ind = 0
        first_ts = []
        last_ts = []
        dur = []
        
        for filename in sorted(fileList):
            print(filename)
      
            #Read the file
            hdr,data = read_TRFile(filename, 0)                
            
            ts_cat=float(hdr['timestamp_s'])+hdr['timestamp_fsec'] 
            time_stamp=time.ctime(ts_cat) 
            print ("Timestamp(s):",ts_cat)            
            print("Record Length:", hdr['length'])        
            print("Pretrigger:", hdr['pretrigger'])
            print(hdr['timestamp_fsec'])      

            first_ts.append(hdr['timestamp_fsec']-(hdr['pretrigger']/hdr['samplerate'])) 
            
            lts = first_ts[ind]+((hdr['length']-1)/hdr['samplerate'])
            
            if lts > 1:
                lts = lts - 1
            
            last_ts.append(lts)
            ind=ind+1
            print(first_ts)
            print(last_ts)
            #s.remove(filename)
            s = filename.rfind('/')
            fname = filename[s+1:]
            os.rename(filename,"/opt/TMSTesting/results/ZeroDeadTime/"+fname) 
        fname = "Zerodeadtime_"+str(TMS_Cfg['Pre_Trig']*10)+"PT.txt"
        with open(fname,'a+') as f:    
            f.write("Segment Length: "+get_seg_Length(TMS_Cfg['Seg_Len'])+"\n")
            f.write("Pre-Trigger: "+str(TMS_Cfg['Pre_Trig']*10)+"\n")            
            for i in range(0,len(first_ts)-1):                          
                
      
                dur.append(first_ts[i+1]-last_ts[i])     
                #if dur[i]<0: # we're just looking at fractional seconds; if the consecutive     
                    #dur[i]+=1  # triggers wrap across a seconds boundary, the subtraction     
                # will result in a negative number; this "borrows" the one.    
              
                dur[i]=round(abs(dur[i]),13)     
                print ("Duration :",dur[i])
                f.write("Duration: "+str(dur[i])+"\n")
            f.write("\n\n")            f.close()   
    #####################################################################################################################################           
         #Trigger Mode test (+ Channels)
    ##################################################################################################################################### 
    if TMS_Cfg['Test_Type'] == 1: 
        
        fileList= glob.glob('*.tr')                        
                          
        meas = ['Ch1+_Pos','Ch2+_Pos','Ch3+_Pos','Ch4+_Pos',
                'Ch1+_Neg','Ch2+_Neg','Ch3+_Neg','Ch4+_Neg',
                'Ch1+_WEn','Ch2+_WEn','Ch3+_WEn','Ch4+_WEn',
                'Ch1+_WEx','Ch2+_WEx','Ch3+_WEx','Ch4+_WEx']
        passfail = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        filescaptured = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        
        
        
        for filename in sorted(fileList):
            print(filename)
      
            #Read the file, including data
            hdr,data = read_TRFile(filename, 1)    
            
#             print(hdr['ch1ortrigger'])
#             print(hdr['ch2ortrigger'])
#             print(hdr['ch3ortrigger'])
#             print(hdr['ch4ortrigger'])
#             
            #This will return sample index 16000, which is actually sample #16001 (the trigger sample)
            trig_sample = int(hdr['length']/2)  
        
            #Positive trigger, Ch1 (400 us segment, 32000 points with 50% pretrig)
            if "positive" in hdr['ch1triggermode'].decode(errors='ignore') and "true" in hdr['ch1ortrigger'].decode(errors='ignore'):
                               
                data0 = numpy.array(data[0])*float(2)/8192
                filescaptured[0]=1   
                
                if data0[trig_sample] >= hdr['ch1triggera'] and data0[trig_sample-1] < hdr['ch1triggera']:
                    passfail[0]=1     
                            
                
            
            #Positive trigger, Ch2 (400 us segment, 32000 points with 50% pretrig)
            elif "positive" in hdr['ch2triggermode'].decode(errors='ignore') and "true" in hdr['ch2ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[1])*float(2)/8192
                filescaptured[1]=1    
                                
                if data0[trig_sample] >= hdr['ch2triggera'] and data0[trig_sample-1] < hdr['ch2triggera']:
                    passfail[1] = 1
                     
        
            #Positive trigger, Ch3 (400 us segment, 32000 points with 50% pretrig)
            elif "positive" in hdr['ch3triggermode'].decode(errors='ignore') and "true" in hdr['ch3ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[2])*float(2)/8192
                filescaptured[2]=1  
                
                if data0[trig_sample] >= hdr['ch3triggera'] and data0[trig_sample-1] < hdr['ch3triggera']:
                    passfail[2] = 1 
                       
             
             
            #Positive trigger, Ch4 (400 us segment, 32000 points with 50% pretrig)
            elif "positive" in hdr['ch4triggermode'].decode(errors='ignore') and "true" in hdr['ch4ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[3])*float(2)/8192
                filescaptured[3]=1     
                
                if data0[trig_sample] >= hdr['ch4triggera'] and data0[trig_sample-1] < hdr['ch4triggera']:
                    passfail[3] = 1
                    
                    
             
            #Negative trigger, Ch1 (400 us segment, 32000 points with 50% pretrig)
            elif "negative" in hdr['ch1triggermode'].decode(errors='ignore') and "true" in hdr['ch1ortrigger'].decode(errors='ignore'):
                               
                data0 = numpy.array(data[0])*float(2)/8192
                filescaptured[4]=1  
                
                if data0[trig_sample] <= hdr['ch1triggera'] and data0[trig_sample-1] > hdr['ch1triggera']:
                    passfail[4]=1     
                             
                
            
            #Negative trigger, Ch2 (400 us segment, 32000 points with 50% pretrig)
            elif "negative" in hdr['ch2triggermode'].decode(errors='ignore') and "true" in hdr['ch2ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[1])*float(2)/8192
                filescaptured[5]=1    
                                
                if data0[trig_sample] <= hdr['ch2triggera'] and data0[trig_sample-1] > hdr['ch2triggera']:
                    passfail[5] = 1
                     
        
            #Negative trigger, Ch3 (400 us segment, 32000 points with 50% pretrig)
            elif "negative" in hdr['ch3triggermode'].decode(errors='ignore') and "true" in hdr['ch3ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[2])*float(2)/8192
                filescaptured[6]=1  
                
                if data0[trig_sample] <= hdr['ch3triggera'] and data0[trig_sample-1] > hdr['ch3triggera']:
                    passfail[6] = 1 
                       
             
             
            #Negative trigger, Ch4 (400 us segment, 32000 points with 50% pretrig)
            elif "negative" in hdr['ch4triggermode'].decode(errors='ignore') and "true" in hdr['ch4ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[3])*float(2)/8192
                filescaptured[7]=1    
                
                if data0[trig_sample] <= hdr['ch4triggera'] and data0[trig_sample-1] > hdr['ch4triggera']:
                    passfail[7] = 1
                            
            
            #Window-Enter trigger, Ch1 (400 us segment, 32000 points with 50% pretrig)
            elif "window-enter" in hdr['ch1triggermode'].decode(errors='ignore') and "true" in hdr['ch1ortrigger'].decode(errors='ignore'):
                               
                data0 = numpy.array(data[0])*float(2)/8192
                filescaptured[8]=1    
                
                if (data0[trig_sample] <= hdr['ch1triggera'] and data0[trig_sample-1] > hdr['ch1triggera']) or (data0[trig_sample] >= hdr['ch1triggerb'] and data0[trig_sample-1] < hdr['ch1triggerb']):
                    passfail[8]=1     
                           
                
            
            #Window-Enter trigger, Ch2 (400 us segment, 32000 points with 50% pretrig)
            elif "window-enter" in hdr['ch2triggermode'].decode(errors='ignore') and "true" in hdr['ch2ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[1])*float(2)/8192
                filescaptured[9]=1 
                                
                if (data0[trig_sample] <= hdr['ch2triggera'] and data0[trig_sample-1] > hdr['ch2triggera']) or (data0[trig_sample] >= hdr['ch2triggerb'] and data0[trig_sample-1] < hdr['ch2triggerb']):
                    passfail[9] = 1
                        
        
            #Window-Enter trigger, Ch3 (400 us segment, 32000 points with 50% pretrig)
            elif "window-enter" in hdr['ch3triggermode'].decode(errors='ignore') and "true" in hdr['ch3ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[2])*float(2)/8192
                filescaptured[10]=1  
                
                if (data0[trig_sample] <= hdr['ch3triggera'] and data0[trig_sample-1] > hdr['ch3triggera']) or (data0[trig_sample] >= hdr['ch3triggerb'] and data0[trig_sample-1] < hdr['ch3triggerb']):
                    passfail[10] = 1 
                       
             
             
            #Window-Enter trigger, Ch4 (400 us segment, 32000 points with 50% pretrig)
            elif "window-enter" in hdr['ch4triggermode'].decode(errors='ignore') and "true" in hdr['ch4ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[3])*float(2)/8192
                filescaptured[11]=1  
                
                if (data0[trig_sample] <= hdr['ch4triggera'] and data0[trig_sample-1] > hdr['ch4triggera']) or (data0[trig_sample] >= hdr['ch4triggerb'] and data0[trig_sample-1] < hdr['ch4triggerb']):
                    passfail[11] = 1
                                      
                    
            #Window-Exit trigger, Ch1 (400 us segment, 32000 points with 50% pretrig)
            elif "window-exit" in hdr['ch1triggermode'].decode(errors='ignore') and "true" in hdr['ch1ortrigger'].decode(errors='ignore'):
                               
                data0 = numpy.array(data[0])*float(2)/8192
                filescaptured[12]=1    
                
                if (data0[trig_sample] >= hdr['ch1triggera'] and data0[trig_sample-1] < hdr['ch1triggera']) or (data0[trig_sample] <= hdr['ch1triggerb'] and data0[trig_sample-1] > hdr['ch1triggerb']):
                    passfail[12]=1     
                           
                
            
            #Window-Exit trigger, Ch2 (400 us segment, 32000 points with 50% pretrig)
            elif "window-exit" in hdr['ch2triggermode'].decode(errors='ignore') and "true" in hdr['ch2ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[1])*float(2)/8192
                filescaptured[13]=1     
                                
                if (data0[trig_sample] >= hdr['ch2triggera'] and data0[trig_sample-1] < hdr['ch2triggera']) or (data0[trig_sample] <= hdr['ch2triggerb'] and data0[trig_sample-1] > hdr['ch2triggerb']):

                    passfail[13] = 1
                    
        
            #Window-Exit trigger, Ch3 (400 us segment, 32000 points with 50% pretrig)
            elif "window-exit" in hdr['ch3triggermode'].decode(errors='ignore') and "true" in hdr['ch3ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[2])*float(2)/8192
                filescaptured[14]=1  
                
                if (data0[trig_sample] >= hdr['ch3triggera'] and data0[trig_sample-1] < hdr['ch3triggera']) or (data0[trig_sample] <= hdr['ch3triggerb'] and data0[trig_sample-1] > hdr['ch3triggerb']):

                    passfail[14] = 1 
                      
             
             
            #Window-Exit trigger, Ch4 (400 us segment, 32000 points with 50% pretrig)
            elif "window-exit" in hdr['ch4triggermode'].decode(errors='ignore') and "true" in hdr['ch4ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[3])*float(2)/8192
                filescaptured[15]=1 
                
                if (data0[trig_sample] >= hdr['ch4triggera'] and data0[trig_sample-1] < hdr['ch4triggera']) or (data0[trig_sample] <= hdr['ch4triggerb'] and data0[trig_sample-1] > hdr['ch4triggerb']):

                    passfail[15] = 1
                        
        
        
            s = filename.rfind('/')
            fname = filename[s+1:]
            os.rename(filename,"/opt/TMSTesting/results/TriggerMode+/"+fname)        
        
        fname = "TriggerMode.txt"
        print(passfail)
        with open(fname,'a+') as f:    
            f.write("Segment Length: "+get_seg_Length(TMS_Cfg['Seg_Len'])+"\n")
            f.write("Pre-Trigger: "+str(TMS_Cfg['Pre_Trig']*10)+"\n")
            for i in range(len(passfail)):             
                if passfail[i] == 1:
                    f.write(meas[i] + ": "+ "Pass, " + "File Captured: " + str(filescaptured[i]) + "\n")
                else:
                    f.write(meas[i] + ": "+ "Fail, " + "File Captured: " + str(filescaptured[i]) + "\n")
                            
        
        os.rename(fname,"/opt/TMSTesting/results/TriggerMode+/"+fname)           
        

    #####################################################################################################################################           
     
    #Trigger Mode test (-Channels)
    ##################################################################################################################################### 
    if TMS_Cfg['Test_Type'] == 2: 
        
        fileList= glob.glob('*.tr')                        
                          
        meas = ['Ch1-_Pos','Ch2-_Pos','Ch3-_Pos','Ch4-_Pos',
                'Ch1-_Neg','Ch2-_Neg','Ch3-_Neg','Ch4-_Neg',
                'Ch1-_WEn','Ch2-_WEn','Ch3-_WEn','Ch4-_WEn',
                'Ch1-_WEx','Ch2-_WEx','Ch3-_WEx','Ch4-_WEx']
        passfail = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        filescaptured = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        
        
        
        for filename in sorted(fileList):
            print(filename)
      
            #Read the file, including data
            hdr,data = read_TRFile(filename, 1)    
            
            
            
#             print(hdr['ch1ortrigger'])
#             print(hdr['ch2ortrigger'])
#             print(hdr['ch3ortrigger'])
#             print(hdr['ch4ortrigger'])
#             
            #This will return sample index 16000, which is actually sample #16001 (the trigger sample)
            trig_sample = int(hdr['length']/2)  
        
            #Positive trigger, Ch1 (400 us segment, 32000 points with 50% pretrig)
            if "positive" in hdr['ch1triggermode'].decode(errors='ignore') and "true" in hdr['ch1ortrigger'].decode(errors='ignore'):
                               
                data0 = numpy.array(data[0])*float(2)/8192
                filescaptured[0]=1   
                
                if data0[trig_sample] >= hdr['ch1triggera'] and data0[trig_sample-1] < hdr['ch1triggera']:
                    passfail[0]=1     
                            
                
            
            #Positive trigger, Ch2 (400 us segment, 32000 points with 50% pretrig)
            elif "positive" in hdr['ch2triggermode'].decode(errors='ignore') and "true" in hdr['ch2ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[1])*float(2)/8192
                filescaptured[1]=1    
                                
                if data0[trig_sample] >= hdr['ch2triggera'] and data0[trig_sample-1] < hdr['ch2triggera']:
                    passfail[1] = 1
                     
        
            #Positive trigger, Ch3 (400 us segment, 32000 points with 50% pretrig)
            elif "positive" in hdr['ch3triggermode'].decode(errors='ignore') and "true" in hdr['ch3ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[2])*float(2)/8192
                filescaptured[2]=1  
                
                if data0[trig_sample] >= hdr['ch3triggera'] and data0[trig_sample-1] < hdr['ch3triggera']:
                    passfail[2] = 1 
                       
             
             
            #Positive trigger, Ch4 (400 us segment, 32000 points with 50% pretrig)
            elif "positive" in hdr['ch4triggermode'].decode(errors='ignore') and "true" in hdr['ch4ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[3])*float(2)/8192
                filescaptured[3]=1     
                
                if data0[trig_sample] >= hdr['ch4triggera'] and data0[trig_sample-1] < hdr['ch4triggera']:
                    passfail[3] = 1
                    
                    
             
            #Negative trigger, Ch1 (400 us segment, 32000 points with 50% pretrig)
            elif "negative" in hdr['ch1triggermode'].decode(errors='ignore') and "true" in hdr['ch1ortrigger'].decode(errors='ignore'):
                               
                data0 = numpy.array(data[0])*float(2)/8192
                filescaptured[4]=1  
                
                if data0[trig_sample] <= hdr['ch1triggera'] and data0[trig_sample-1] > hdr['ch1triggera']:
                    passfail[4]=1     
                             
                
            
            #Negative trigger, Ch2 (400 us segment, 32000 points with 50% pretrig)
            elif "negative" in hdr['ch2triggermode'].decode(errors='ignore') and "true" in hdr['ch2ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[1])*float(2)/8192
                filescaptured[5]=1    
                                
                if data0[trig_sample] <= hdr['ch2triggera'] and data0[trig_sample-1] > hdr['ch2triggera']:
                    passfail[5] = 1
                     
        
            #Negative trigger, Ch3 (400 us segment, 32000 points with 50% pretrig)
            elif "negative" in hdr['ch3triggermode'].decode(errors='ignore') and "true" in hdr['ch3ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[2])*float(2)/8192
                filescaptured[6]=1  
                
                if data0[trig_sample] <= hdr['ch3triggera'] and data0[trig_sample-1] > hdr['ch3triggera']:
                    passfail[6] = 1 
                       
             
             
            #Negative trigger, Ch4 (400 us segment, 32000 points with 50% pretrig)
            elif "negative" in hdr['ch4triggermode'].decode(errors='ignore') and "true" in hdr['ch4ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[3])*float(2)/8192
                filescaptured[7]=1    
                
                if data0[trig_sample] <= hdr['ch4triggera'] and data0[trig_sample-1] > hdr['ch4triggera']:
                    passfail[7] = 1
                            
            
            #Window-Enter trigger, Ch1 (400 us segment, 32000 points with 50% pretrig)
            elif "window-enter" in hdr['ch1triggermode'].decode(errors='ignore') and "true" in hdr['ch1ortrigger'].decode(errors='ignore'):
                               
                data0 = numpy.array(data[0])*float(2)/8192
                filescaptured[8]=1    
                
                if (data0[trig_sample] <= hdr['ch1triggera'] and data0[trig_sample-1] > hdr['ch1triggera']) or (data0[trig_sample] >= hdr['ch1triggerb'] and data0[trig_sample-1] < hdr['ch1triggerb']):
                    passfail[8]=1     
                           
                
            
            #Window-Enter trigger, Ch2 (400 us segment, 32000 points with 50% pretrig)
            elif "window-enter" in hdr['ch2triggermode'].decode(errors='ignore') and "true" in hdr['ch2ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[1])*float(2)/8192
                filescaptured[9]=1 
                                
                if (data0[trig_sample] <= hdr['ch2triggera'] and data0[trig_sample-1] > hdr['ch2triggera']) or (data0[trig_sample] >= hdr['ch2triggerb'] and data0[trig_sample-1] < hdr['ch2triggerb']):
                    passfail[9] = 1
                        
        
            #Window-Enter trigger, Ch3 (400 us segment, 32000 points with 50% pretrig)
            elif "window-enter" in hdr['ch3triggermode'].decode(errors='ignore') and "true" in hdr['ch3ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[2])*float(2)/8192
                filescaptured[10]=1  
                
                if (data0[trig_sample] <= hdr['ch3triggera'] and data0[trig_sample-1] > hdr['ch3triggera']) or (data0[trig_sample] >= hdr['ch3triggerb'] and data0[trig_sample-1] < hdr['ch3triggerb']):
                    passfail[10] = 1 
                       
             
             
            #Window-Enter trigger, Ch4 (400 us segment, 32000 points with 50% pretrig)
            elif "window-enter" in hdr['ch4triggermode'].decode(errors='ignore') and "true" in hdr['ch4ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[3])*float(2)/8192
                filescaptured[11]=1  
                
                if (data0[trig_sample] <= hdr['ch4triggera'] and data0[trig_sample-1] > hdr['ch4triggera']) or (data0[trig_sample] >= hdr['ch4triggerb'] and data0[trig_sample-1] < hdr['ch4triggerb']):
                    passfail[11] = 1
                                      
                    
            #Window-Exit trigger, Ch1 (400 us segment, 32000 points with 50% pretrig)
            elif "window-exit" in hdr['ch1triggermode'].decode(errors='ignore') and "true" in hdr['ch1ortrigger'].decode(errors='ignore'):
                               
                data0 = numpy.array(data[0])*float(2)/8192
                filescaptured[12]=1    
                
                if (data0[trig_sample] >= hdr['ch1triggera'] and data0[trig_sample-1] < hdr['ch1triggera']) or (data0[trig_sample] <= hdr['ch1triggerb'] and data0[trig_sample-1] > hdr['ch1triggerb']):
                    passfail[12]=1     
                           
                
            
            #Window-Exit trigger, Ch2 (400 us segment, 32000 points with 50% pretrig)
            elif "window-exit" in hdr['ch2triggermode'].decode(errors='ignore') and "true" in hdr['ch2ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[1])*float(2)/8192
                filescaptured[13]=1     
                                
                if (data0[trig_sample] >= hdr['ch2triggera'] and data0[trig_sample-1] < hdr['ch2triggera']) or (data0[trig_sample] <= hdr['ch2triggerb'] and data0[trig_sample-1] > hdr['ch2triggerb']):

                    passfail[13] = 1
                    
        
            #Window-Exit trigger, Ch3 (400 us segment, 32000 points with 50% pretrig)
            elif "window-exit" in hdr['ch3triggermode'].decode(errors='ignore') and "true" in hdr['ch3ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[2])*float(2)/8192
                filescaptured[14]=1  
                
                if (data0[trig_sample] >= hdr['ch3triggera'] and data0[trig_sample-1] < hdr['ch3triggera']) or (data0[trig_sample] <= hdr['ch3triggerb'] and data0[trig_sample-1] > hdr['ch3triggerb']):

                    passfail[14] = 1 
                      
             
             
            #Window-Exit trigger, Ch4 (400 us segment, 32000 points with 50% pretrig)
            elif "window-exit" in hdr['ch4triggermode'].decode(errors='ignore') and "true" in hdr['ch4ortrigger'].decode(errors='ignore'):
                
                data0 = numpy.array(data[3])*float(2)/8192
                filescaptured[15]=1 
                
                if (data0[trig_sample] >= hdr['ch4triggera'] and data0[trig_sample-1] < hdr['ch4triggera']) or (data0[trig_sample] <= hdr['ch4triggerb'] and data0[trig_sample-1] > hdr['ch4triggerb']):

                    passfail[15] = 1
                        
        
        
            s = filename.rfind('/')
            fname = filename[s+1:]
            os.rename(filename,"/opt/TMSTesting/results/TriggerMode-/"+fname)        
        
        fname = "TriggerMode.txt"
        print(passfail)
        with open(fname,'a+') as f:    
            f.write("Segment Length: "+get_seg_Length(TMS_Cfg['Seg_Len'])+"\n")
            f.write("Pre-Trigger: "+str(TMS_Cfg['Pre_Trig']*10)+"\n")
            for i in range(len(passfail)):             
                if passfail[i] == 1:
                    f.write(meas[i] + ": "+ "Pass, " + "File Captured: " + str(filescaptured[i]) + "\n")
                else:
                    f.write(meas[i] + ": "+ "Fail, " + "File Captured: " + str(filescaptured[i]) + "\n")
                            
        
        os.rename(fname,"/opt/TMSTesting/results/TriggerMode-/"+fname)       

    #####################################################################################################################################           
     
    #Logic Trigger test 
    ##################################################################################################################################### 
    if TMS_Cfg['Test_Type'] == 3:     
        
        fileList= glob.glob('*.tr')                        
                          
        meas = ['Ch1_OR','Ch2_OR','Ch3_OR','Ch4_OR',
                'Ch12_AND','Ch13_AND','Ch14_AND','Ch23_AND',
                'Ch24_AND','Ch34_AND','Ch123_AND', 'Ch124_AND','Ch134_AND',
                'Ch234_AND','Ch1234_AND','Ch14_OR_Ch23_AND','Ch23_OR_Ch14_AND']
        passfail = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        filescaptured = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        
        
        
        
        for filename in sorted(fileList):
            print(filename)
      
            #Read the file, including data
            hdr,data = read_TRFile(filename, 1)    
            
            ls = [hdr['ch1ortrigger'].decode(errors='ignore'),hdr['ch2ortrigger'].decode(errors='ignore'),
              hdr['ch3ortrigger'].decode(errors='ignore'),hdr['ch4ortrigger'].decode(errors='ignore'),
              hdr['ch1andtrigger'].decode(errors='ignore'),hdr['ch2andtrigger'].decode(errors='ignore'),
              hdr['ch3andtrigger'].decode(errors='ignore'),hdr['ch4andtrigger'].decode(errors='ignore')]
            
            lsg = ["true" in ls[0], "true" in ls[1], "true" in ls[2], "true" in ls[3],
                   "true" in ls[4], "true" in ls[5], "true" in ls[6], "true" in ls[7]]           
                       
            
            #Ch1 OR
            if lsg == [True, False, False, False, False, False, False, False]:
                data0 = numpy.array(data[0])*float(2)/8192
                filescaptured[0]=1 
                if(numpy.abs(numpy.amax(data0))) > .2:
                    passfail[0] = 1
                    
            #Ch2 OR
            if lsg == [False, True, False, False, False, False, False, False]:
                data0 = numpy.array(data[1])*float(2)/8192
                filescaptured[1]=1 
                if(numpy.abs(numpy.amax(data0))) > .2:
                    passfail[1] = 1        
            
            #Ch3 OR
            if lsg == [False, False, True, False, False, False, False, False]:
                data0 = numpy.array(data[2])*float(2)/8192
                filescaptured[2]=1 
                if(numpy.abs(numpy.amax(data0))) > .2:
                    passfail[2] = 1        
                    
            #Ch4 OR
            if lsg == [False, False, False, True, False, False, False, False]:
                data0 = numpy.array(data[3])*float(2)/8192
                filescaptured[3]=1 
                if(numpy.abs(numpy.amax(data0))) > .2:
                    passfail[3] = 1    
                    
            #Ch12 AND            
            if lsg == [False, False, False, False, True, True, False, False]:
                data0 = numpy.array(data[0])*float(2)/8192
                data1 = numpy.array(data[1])*float(2)/8192
                filescaptured[4]=1 
                if(numpy.abs(numpy.amax(data0)) > .2 and numpy.abs(numpy.amax(data1)) > .2):
                    passfail[4] = 1
            
            #Ch13 AND            
            if lsg == [False, False, False, False, True, False, True, False]:
                data0 = numpy.array(data[0])*float(2)/8192
                data1 = numpy.array(data[2])*float(2)/8192
                filescaptured[5]=1 
                if(numpy.abs(numpy.amax(data0)) > .2 and numpy.abs(numpy.amax(data1)) > .2):
                    passfail[5] = 1
            
            #Ch14 AND            
            if lsg == [False, False, False, False, True, False, False, True]:
                data0 = numpy.array(data[0])*float(2)/8192
                data1 = numpy.array(data[3])*float(2)/8192
                filescaptured[6]=1 
                if(numpy.abs(numpy.amax(data0)) > .2 and numpy.abs(numpy.amax(data1)) > .2):
                    passfail[6] = 1    
                    
            #Ch23 AND            
            if lsg == [False, False, False, False, False, True, True, False]:
                data0 = numpy.array(data[1])*float(2)/8192
                data1 = numpy.array(data[2])*float(2)/8192
                filescaptured[7]=1 
                if(numpy.abs(numpy.amax(data0)) > .2 and numpy.abs(numpy.amax(data1)) > .2):
                    passfail[7] = 1            
                
            #Ch24 AND            
            if lsg == [False, False, False, False, False, True, False, True]:
                data0 = numpy.array(data[1])*float(2)/8192
                data1 = numpy.array(data[3])*float(2)/8192
                filescaptured[8]=1 
                if(numpy.abs(numpy.amax(data0)) > .2 and numpy.abs(numpy.amax(data1)) > .2):
                    passfail[8] = 1         
                
            #Ch34 AND            
            if lsg == [False, False, False, False, False, False, True, True]:
                data0 = numpy.array(data[2])*float(2)/8192
                data1 = numpy.array(data[3])*float(2)/8192
                filescaptured[9]=1 
                if(numpy.abs(numpy.amax(data0)) > .2 and numpy.abs(numpy.amax(data1)) > .2):
                    passfail[9] = 1          
                
            #Ch123 AND            
            if lsg == [False, False, False, False, True, True, True,False]:
                data0 = numpy.array(data[0])*float(2)/8192
                data1 = numpy.array(data[1])*float(2)/8192
                data2 = numpy.array(data[2])*float(2)/8192
                filescaptured[10]=1 
                if(numpy.abs(numpy.amax(data0)) > .2 and numpy.abs(numpy.amax(data1)) > .2 and numpy.abs(numpy.amax(data2)) > .2):
                    passfail[10] = 1   
                    
            #Ch124 AND            
            if lsg == [False, False, False, False, True, True, False, True]:
                data0 = numpy.array(data[0])*float(2)/8192
                data1 = numpy.array(data[1])*float(2)/8192
                data2 = numpy.array(data[3])*float(2)/8192
                filescaptured[11]=1 
                if(numpy.abs(numpy.amax(data0)) > .2 and numpy.abs(numpy.amax(data1)) > .2 and numpy.abs(numpy.amax(data2)) > .2):
                    passfail[11] = 1   
            
            #Ch134 AND            
            if lsg == [False, False, False, False, True, False, True, True]:
                data0 = numpy.array(data[0])*float(2)/8192
                data1 = numpy.array(data[2])*float(2)/8192
                data2 = numpy.array(data[3])*float(2)/8192
                filescaptured[12]=1 
                if(numpy.abs(numpy.amax(data0)) > .2 and numpy.abs(numpy.amax(data1)) > .2 and numpy.abs(numpy.amax(data2)) > .2):
                    passfail[12] = 1        
                
            #Ch234 AND            
            if lsg == [False, False, False, False, False, True, True, True]:
                data0 = numpy.array(data[1])*float(2)/8192
                data1 = numpy.array(data[2])*float(2)/8192
                data2 = numpy.array(data[3])*float(2)/8192
                filescaptured[13]=1 
                if(numpy.abs(numpy.amax(data0)) > .2 and numpy.abs(numpy.amax(data1)) > .2 and numpy.abs(numpy.amax(data2)) > .2):
                    passfail[13] = 1           
                
            #Ch1234 AND            
            if lsg == [False, False, False, False, True, True, True, True]:
                data0 = numpy.array(data[0])*float(2)/8192
                data1 = numpy.array(data[1])*float(2)/8192
                data2 = numpy.array(data[2])*float(2)/8192
                data3 = numpy.array(data[3])*float(2)/8192
                filescaptured[14]=1 
                if(numpy.abs(numpy.amax(data0)) > .2 and numpy.abs(numpy.amax(data1)) > .2 and numpy.abs(numpy.amax(data2)) > .2 and numpy.abs(numpy.amax(data3)) > .2):
                    passfail[14] = 1        
            
            
            #Ch14 OR Ch23 AND
            if lsg == [True, False, False, True, False, True, True, False]:
                data0 = numpy.array(data[0])*float(2)/8192
                data1 = numpy.array(data[1])*float(2)/8192
                data2 = numpy.array(data[2])*float(2)/8192
                data3 = numpy.array(data[3])*float(2)/8192
                filescaptured[15]=1 
                if((numpy.abs(numpy.amax(data0)) > .2 and numpy.abs(numpy.amax(data1)) > .2 and numpy.abs(numpy.amax(data2)) > .2) 
                   or (numpy.abs(numpy.amax(data3)) > .2 and numpy.abs(numpy.amax(data1)) > .2 and numpy.abs(numpy.amax(data2)) > .2)): 
                    passfail[15] = 1    
                    
            #Ch23 OR Ch14 AND
            if lsg == [False, True, True, False, True, False, False, True]:
                data0 = numpy.array(data[0])*float(2)/8192
                data1 = numpy.array(data[1])*float(2)/8192
                data2 = numpy.array(data[2])*float(2)/8192
                data3 = numpy.array(data[3])*float(2)/8192
                filescaptured[16]=1 
                if((numpy.abs(numpy.amax(data1)) > .2 and numpy.abs(numpy.amax(data0)) > .2 and numpy.abs(numpy.amax(data3)) > .2) 
                   or (numpy.abs(numpy.amax(data2)) > .2 and numpy.abs(numpy.amax(data0)) > .2 and numpy.abs(numpy.amax(data3)) > .2)): 
                    passfail[16] = 1        
                
                
            s = filename.rfind('/')
            fname = filename[s+1:]
            os.rename(filename,"/opt/TMSTesting/results/TriggerLogic/"+fname)        
        
        fname = "TriggerLogic.txt"
        print(passfail)
        with open(fname,'a+') as f:    
            f.write("Segment Length: "+get_seg_Length(TMS_Cfg['Seg_Len'])+"\n")
            f.write("Pre-Trigger: "+str(TMS_Cfg['Pre_Trig']*10)+"\n")
            for i in range(len(passfail)):             
                if passfail[i] == 1:
                    f.write(meas[i] + ": "+ "Pass, " + "File Captured: " + str(filescaptured[i]) + "\n")
                else:
                    f.write(meas[i] + ": "+ "Fail, " + "File Captured: " + str(filescaptured[i]) + "\n")
                            
        
        os.rename(fname,"/opt/TMSTesting/results/TriggerLogic/"+fname)       


# 
#         ts = 1.0/hdr["samplerate"]
#         timevec = numpy.multiply(range(len(data0)),ts)
#            
#         trace = Scatter(
#             x = timevec,
#             y = data0,
#         mode = 'lines',
#         name = 'Ch. 1',
#         )
#            
#         
#         plotly.offline.plot({
#             'data': trace,
#             'layout': Layout(
#             title="Data"
#             )
#         })








