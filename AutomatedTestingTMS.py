


def generate_CFG(testType,numTrig,segLen, preTrig,trigA, trigB, trigMode, trigOR, trigAND, Hyst):
    # Options for pulldowns in the TMS menu; update this when the menu changes. Not used for testing, used for presentation of results
    SegLens=['10 µs','20 µs','50 µs','100 µs','200 µs','500 us','1 ms','2 ms','5 ms','10 ms','20 ms','50 ms','100 ms', '200 ms','500 ms','1 s', '2 s' ]
    #SegLens=['8 µs','20 µs','40 µs','80 µs','200 µs','400 µs','1 ms','2 ms','4 ms','8 ms','20 ms','40 ms','80 ms', '200 ms','400 ms','800 ms', '2 s' ]
    PreTrigs=['0', '10', '20', '30', '40', '50', '60', '70', '80', '90', '100']
    Ranges=['0.2', '2', '20', '200']
    Zins=['1 Meg', '50']
    Cpls=['DC', 'AC', 'GND']
    TRIGMODES=['POS', 'NEG', 'WINENTER', 'WINEXIT']
    
    
      
    # Actual data to be sent to TMS for config    
    TMS_Cfg = {}
    TMS_Cfg['Num_Trig'] = numTrig
    TMS_Cfg['Seg_Len'] = segLen
    TMS_Cfg['Pre_Trig'] = preTrig
    TMS_Cfg['Header_Name'] = 'Deadtime'
    
    TMS_Cfg['Ch1'] = {'Range': 1, 'Zin': 0, 'CPL': 0, 'TrgA': trigA[0], 'TrgB': trigB[0], 'Hyst': Hyst[0], 'TrigMode': trigMode[0], 'TrigOr': trigOR[0], 'TrigAnd': trigAND[0]}
    TMS_Cfg['Ch2'] = {'Range': 1, 'Zin': 0, 'CPL': 0, 'TrgA': trigA[1], 'TrgB': trigB[1], 'Hyst': Hyst[1], 'TrigMode': trigMode[1], 'TrigOr': trigOR[1], 'TrigAnd': trigAND[1]}
    TMS_Cfg['Ch3'] = {'Range': 1, 'Zin': 0, 'CPL': 0, 'TrgA': trigA[2], 'TrgB': trigB[2], 'Hyst': Hyst[2], 'TrigMode': trigMode[2], 'TrigOr': trigOR[2], 'TrigAnd': trigAND[2]}
    TMS_Cfg['Ch4'] = {'Range': 1, 'Zin': 0, 'CPL': 0, 'TrgA': trigA[3], 'TrgB': trigB[3], 'Hyst': Hyst[3], 'TrigMode': trigMode[3], 'TrigOr': trigOR[3], 'TrigAnd': trigAND[3]}
    
    TMS_Cfg['Test_Type'] = testType
   
    
    return TMS_Cfg